/// <reference lib="webworker" />

addEventListener('message', ({ data }) => {
  const randomArray = [];
  const length = data;

  for (let i = 0; i < length; i++) {
    randomArray.push(Math.random());
  }

  const sortedArray = randomArray.sort();
  postMessage(sortedArray);
});
