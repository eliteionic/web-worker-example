import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private sortWorker: Worker;

  constructor() {}

  doSort() {
    const randomArray = [];

    console.log('create array');

    for (let i = 0; i < 1000000; i++) {
      randomArray.push(Math.random());
    }

    console.log('start sort');

    randomArray.sort();

    console.log('sort finished');
  }

  doSortWithWorker() {
    if (typeof Worker !== 'undefined') {
      const worker = new Worker('../workers/sort.worker', { type: 'module' });
      worker.onmessage = ({ data }) => {
        console.log(`Done!: ${data}`);
      };
      worker.postMessage(1000000);
    } else {
      // Web workers not supported, use non worker sort
      this.doSort();
    }
  }
}
